import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import React, { Component } from "react";
import ReduxThunk from "redux-thunk";
import { View, StatusBar } from "react-native";

import Router from "./Router";
import AlertProvider from "./AlertProvider";
import reducers from "./reducers";

class App extends Component {
	render() {
		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

		return (
			<View style={{ flex: 1 }}>
				<StatusBar barStyle="light-content" />

				<Provider store={store}>
					<View style={{ flex: 1 }}>
						<Router />
						<AlertProvider />
					</View>
				</Provider>
			</View>
		);
	}
}
export default App;

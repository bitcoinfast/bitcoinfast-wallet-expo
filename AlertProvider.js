import React, { Component } from "react";
import DropdownAlert from "react-native-dropdownalert";
import { connect } from "react-redux";
import { Platform } from "react-native";

import { removeAlert } from "./actions";

class AlertProvider extends Component {
	componentDidUpdate = prevProps => {
		const { alert } = this.props.wallet;

		if (alert && alert !== prevProps.wallet.alert) {
			this.dropdown.alertWithType(alert.type, alert.title, alert.message);
		}
	};

	render() {
		return (
			<DropdownAlert
				ref={ref => {
					this.dropdown = ref;
				}}
				inactiveStatusBarStyle="light-content"
				onClose={() => this.props.removeAlert()}
				defaultContainer={{ paddingTop: 24 }}
			/>
		);
	}
}

const mapStateToProps = ({ wallet }) => {
	return { wallet };
};

export default connect(
	mapStateToProps,
	{
		removeAlert
	}
)(AlertProvider);

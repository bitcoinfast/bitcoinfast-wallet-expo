import { combineReducers } from 'redux';
import SignupReducer from './SignupReducer';
import LoginReducer from './LoginReducer';
import WalletReducer from './WalletReducer';
import SendFormReducer from './SendFormReducer';
import KeypairReducer from './KeypairReducer';

export default combineReducers({
  signup: SignupReducer,
  login: LoginReducer,
  wallet: WalletReducer,
  sendForm: SendFormReducer,
  keypair: KeypairReducer
});

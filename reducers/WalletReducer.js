import {
	LOAD_WALLET_SUCCESS,
	NETWORK_FAIL,
	REFRESH_WALLET,
	NEW_ADDRESS_SUCCESS,
	NEW_ADDRESS,
	LOGOUT,
	SEND_FORM_SUCCESS,
	REMOVE_ALERT,
	NEW_PASSWORD_CONFIRMATION_CHANGED,
	NEW_PASSWORD_CHANGED,
	CHANGE_PASSWORD_SUBMITTED,
	CHANGE_PASSWORD_FAIL,
	CHANGE_PASSWORD_SUCCESS,
	IMPORT_FORM_FAIL,
	IMPORT_FORM_SUCCESS,
	IMPORT_FORM_SUBMITTED,
	IMPORT_FORM_TEXT_CHANGED,
	TWO_FACTOR_TOGGLED,
	TWO_FACTOR_TOGGLE_SUCCESS,
	TWO_FACTOR_TOGGLE_FAIL
} from "../actions/types";

const INITIAL_STATE = {
	address: "loading...",
	backupKey: null,
	balance: "loading...",
	twoFactorEnabled: false,
	transactions: "loading...",
	addresses: "loading...",
	refreshing: false,
	error: "",
	alert: "",
	newPassword: "",
	newPasswordConfirmation: "",
	changePasswordLoading: false,
	changePasswordError: "",
	importFormText: "",
	importFormLoading: false,
	importFormError: "",
	loading: true
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case TWO_FACTOR_TOGGLE_SUCCESS:
			return {
				...state,
				twoFactorEnabled: action.payload.data.two_factor_enabled,
				backupKey: action.payload.data.google_secret
			};
		case TWO_FACTOR_TOGGLE_FAIL:
			return {
				...state,
				twoFactorEnabled: !state.twoFactorEnabled,
				alert: {
					type: "error",
					title: "Error",
					message: "Something went wrong, please try again later."
				}
			};
		case TWO_FACTOR_TOGGLED:
			return {
				...state,
				twoFactorEnabled: !state.twoFactorEnabled
			};
		case IMPORT_FORM_SUCCESS:
			return {
				...state,
				importFormLoading: false,
				importFormError: "",
				addresses: action.payload.data.addresses,
				alert: {
					type: "success",
					title: "Success",
					message: "Successfully imported private key."
				}
			};
		case IMPORT_FORM_FAIL:
			return {
				...state,
				importFormLoading: false,
				importFormError: action.payload.data.message
			};
		case IMPORT_FORM_SUBMITTED:
			return {
				...state,
				importFormLoading: true
			};
		case IMPORT_FORM_TEXT_CHANGED:
			return {
				...state,
				importFormText: action.payload
			};
		case CHANGE_PASSWORD_SUCCESS:
			return {
				...state,
				changePasswordError: "",
				changePasswordLoading: false,
				newPassword: "",
				newPasswordConfirmation: "",
				alert: {
					type: "success",
					title: "Success",
					message: "Password changed."
				}
			};
		case CHANGE_PASSWORD_FAIL:
			return {
				...state,
				changePasswordError: action.payload.data,
				changePasswordLoading: false
			};
		case CHANGE_PASSWORD_SUBMITTED:
			return { ...state, changePasswordLoading: true };
		case NEW_PASSWORD_CONFIRMATION_CHANGED:
			return { ...state, newPasswordConfirmation: action.payload };
		case NEW_PASSWORD_CHANGED:
			return { ...state, newPassword: action.payload };
		case REMOVE_ALERT:
			return { ...state, alert: "" };
		case SEND_FORM_SUCCESS:
			return {
				...state,
				balance: `${action.payload.data.balance} BCF`,
				transactions: action.payload.data.transactions.reverse(),
				alert: {
					type: "success",
					title: "Success",
					message: "Transaction completed."
				}
			};
		case LOGOUT:
			return INITIAL_STATE;
		case NEW_ADDRESS_SUCCESS:
			return {
				...state,
				address: action.payload.new_address,
				addresses: action.payload.addresses
			};
		case NEW_ADDRESS:
			return { ...state, address: "loading..." };
		case REFRESH_WALLET:
			return { ...state, refreshing: true };
		case NETWORK_FAIL:
			return {
				...INITIAL_STATE,
				alert: {
					type: "error",
					title: "Error",
					message: "Service unavailable, please try again later."
				}
			};
		case LOAD_WALLET_SUCCESS:
			return {
				...state,
				address: action.payload.address,
				backupKey: action.payload.backup_key,
				balance: action.payload.balance,
				usdBalance: action.payload.usd_balance,
				twoFactorEnabled: action.payload.two_factor_enabled,
				transactions: action.payload.transactions.reverse(),
				addresses: action.payload.addresses.reverse(),
				refreshing: false,
				loading: false,
				error: "",
				usdRate: action.payload.usd_rate
			};
		default:
			return state;
	}
};

import {
  LOGIN_EMAIL_CHANGED,
  LOGIN_PASSWORD_CHANGED,
  LOGIN_FORM_SUBMITTED,
  LOGIN_SUCCESS,
  NETWORK_FAIL,
  LOGIN_FAIL,
  LOGIN_AUTHENTICATOR_CODE_CHANGED,
  LOGIN_REQUIRE_AUTHENTICATOR_CODE,
  LOGIN_AUTHENTICATOR_FAIL,
  LOGIN_TWO_FACTOR_UNMOUNT,
  SIGNUP_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
  error: '',
  loading: false,
  authenticatorCode: '',
  authenticatorError: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SIGNUP_SUCCESS:
      return INITIAL_STATE;
    case LOGIN_TWO_FACTOR_UNMOUNT:
      return { ...state, authenticatorError: '', authenticatorCode: '' };
    case LOGIN_AUTHENTICATOR_FAIL:
      return { ...state, loading: false, authenticatorError: 'Incorrect 2fa code.' };
    case LOGIN_AUTHENTICATOR_CODE_CHANGED:
      return { ...state, authenticatorCode: action.payload };
    case LOGIN_REQUIRE_AUTHENTICATOR_CODE:
      return { ...state, loading: false };
    case LOGIN_SUCCESS:
      return INITIAL_STATE;
    case NETWORK_FAIL:
      return INITIAL_STATE;
    case LOGIN_FAIL:
      return { ...state, loading: false, error: 'Incorrect email or password.' };
    case LOGIN_FORM_SUBMITTED:
      return { ...state, loading: true, error: '' };
    case LOGIN_PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case LOGIN_EMAIL_CHANGED:
      return { ...state, email: action.payload };
    default:
      return state;
  }
};

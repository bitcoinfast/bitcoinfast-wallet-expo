import {
  SIGNUP_EMAIL_CHANGED,
  SIGNUP_PASSWORD_CHANGED,
  SIGNUP_PASSWORD_CONFIRMATION_CHANGED,
  SIGNUP_TOS_CHANGED,
  SIGNUP_FORM_SUBMITTED,
  SIGNUP_FAIL,
  NETWORK_FAIL,
  SIGNUP_SUCCESS,
  LOGIN_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  email: '',
  password: '',
  passwordConfirmation: '',
  tos: false,
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return INITIAL_STATE;
    case NETWORK_FAIL:
      return INITIAL_STATE;
    case SIGNUP_SUCCESS:
      return INITIAL_STATE;
    case SIGNUP_FAIL:
      return { ...state, loading: false, error: action.payload };
    case SIGNUP_FORM_SUBMITTED:
      return { ...state, loading: true, error: '' };
    case SIGNUP_TOS_CHANGED:
      return { ...state, tos: !state.tos };
    case SIGNUP_PASSWORD_CONFIRMATION_CHANGED:
      return { ...state, passwordConfirmation: action.payload };
    case SIGNUP_PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case SIGNUP_EMAIL_CHANGED:
      return { ...state, email: action.payload };
    default:
      return state;
  }
};

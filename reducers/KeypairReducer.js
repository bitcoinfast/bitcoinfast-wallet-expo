import {
  LOAD_KEYPAIR,
  LOAD_KEYPAIR_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  address: 'loading...',
  privateKey: 'loading...'
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOAD_KEYPAIR:
      return INITIAL_STATE;
    case LOAD_KEYPAIR_SUCCESS:
      return {
        ...state,
        address: action.payload.data.address,
        privateKey: action.payload.data.private_key
      };
    default:
      return state;
  }
};

import {
	SEND_FORM_ADDRESS_CHANGED,
	SEND_FORM_AMOUNT_CHANGED,
	SEND_FORM_SUBMITTED,
	SEND_FORM_FAIL,
	SEND_FORM_SUCCESS,
	SEND_FORM_NETWORK_FAIL,
	SEND_FORM_USD_CHANGED
} from "../actions/types";

const INITIAL_STATE = {
	address: "",
	amount: "",
	usd: "",
	error: "",
	loading: false
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SEND_FORM_SUCCESS:
			return {
				...state,
				address: "",
				amount: "",
				error: "",
				loading: false
			};
		case SEND_FORM_FAIL:
			return { ...state, loading: false, error: action.payload };
		case SEND_FORM_NETWORK_FAIL:
			return INITIAL_STATE;
		case SEND_FORM_SUBMITTED:
			return { ...state, loading: true, error: "" };
		case SEND_FORM_ADDRESS_CHANGED:
			return { ...state, address: action.payload };
		case SEND_FORM_AMOUNT_CHANGED:
			return {
				...state,
				amount: action.payload.amount,
				usd: action.payload.usd
			};
		case SEND_FORM_USD_CHANGED:
			return {
				...state,
				amount: action.payload.amount,
				usd: action.payload.usd
			};
		default:
			return state;
	}
};

import { Actions } from "react-native-router-flux";
import { SEND_FORM_ADDRESS_CHANGED } from "./types";

export const openScanner = () => {
	return dispatch => {
		Actions.scanner();
	};
};

export const setAddressAndReturn = address => {
	return dispatch => {
		Actions.send();
		dispatch({
			type: SEND_FORM_ADDRESS_CHANGED,
			payload: address
		});
	};
};

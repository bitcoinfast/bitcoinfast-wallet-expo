import { AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import axios from "axios";
import {
	LOAD_WALLET_SUCCESS,
	NETWORK_FAIL,
	REFRESH_WALLET,
	NEW_ADDRESS_SUCCESS,
	NEW_ADDRESS,
	LOGOUT,
	REMOVE_ALERT,
	NEW_PASSWORD_CONFIRMATION_CHANGED,
	NEW_PASSWORD_CHANGED,
	CHANGE_PASSWORD_SUBMITTED,
	CHANGE_PASSWORD_FAIL,
	CHANGE_PASSWORD_SUCCESS,
	IMPORT_FORM_FAIL,
	IMPORT_FORM_SUCCESS,
	IMPORT_FORM_SUBMITTED,
	IMPORT_FORM_TEXT_CHANGED,
	TWO_FACTOR_TOGGLED,
	TWO_FACTOR_TOGGLE_SUCCESS,
	TWO_FACTOR_TOGGLE_FAIL,
	LOGIN_TWO_FACTOR_UNMOUNT
} from "./types";

export const unmountTwoFactor = () => {
	return {
		type: LOGIN_TWO_FACTOR_UNMOUNT
	};
};

export const twoFactorToggled = toggled => {
	return dispatch => {
		dispatch({ type: TWO_FACTOR_TOGGLED });

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			if (toggled) {
				// turn off
				axios
					.delete("https://ios.bitcoinfast.co/two_factor/blablabla", {
						headers: {
							Authorization: `Bearer ${jwt}`
						}
					})
					.then(response => {
						twoFactorSuccess(dispatch, response);
					})
					.catch(({ response }) => {
						console.log(response);
						twoFactorFail(dispatch);
					});
			} else {
				// turn on
				axios
					.post(
						"https://ios.bitcoinfast.co/two_factor",
						{},
						{
							headers: {
								Authorization: `Bearer ${jwt}`
							}
						}
					)
					.then(response => {
						twoFactorSuccess(dispatch, response);
					})
					.catch(({ response }) => {
						console.log(response);
						twoFactorFail(dispatch);
					});
			}
		});
	};
};

const twoFactorSuccess = (dispatch, response) => {
	dispatch({
		type: TWO_FACTOR_TOGGLE_SUCCESS,
		payload: response
	});
};

const twoFactorFail = dispatch => {
	dispatch({
		type: TWO_FACTOR_TOGGLE_FAIL
	});
};

export const importFormSubmitted = key => {
	return dispatch => {
		dispatch({ type: IMPORT_FORM_SUBMITTED });

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			axios
				.post(
					"https://ios.bitcoinfast.co/private_keys",
					{
						wallet: {
							private_key: key
						}
					},
					{
						headers: {
							Authorization: `Bearer ${jwt}`
						}
					}
				)
				.then(response => {
					importFormSuccess(dispatch, response);
				})
				.catch(({ response }) => {
					console.log(response);
					importFormFail(dispatch, response);
				});
		});
	};
};

const importFormSuccess = (dispatch, response) => {
	dispatch({
		type: IMPORT_FORM_SUCCESS,
		payload: response
	});
};

const importFormFail = (dispatch, response) => {
	dispatch({
		type: IMPORT_FORM_FAIL,
		payload: response
	});
};

export const importFormTextChanged = text => {
	return {
		type: IMPORT_FORM_TEXT_CHANGED,
		payload: text
	};
};

export const changePasswordSubmitted = ({
	newPassword,
	newPasswordConfirmation
}) => {
	return dispatch => {
		dispatch({ type: CHANGE_PASSWORD_SUBMITTED });

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			axios
				.put(
					"https://ios.bitcoinfast.co/wallets/blablabla",
					{
						wallet: {
							password: newPassword,
							password_confirmation: newPasswordConfirmation
						}
					},
					{
						headers: {
							Authorization: `Bearer ${jwt}`
						}
					}
				)
				.then(() => {
					changePasswordSuccess(dispatch);
				})
				.catch(({ response }) => {
					console.log(response);
					changePasswordFail(dispatch, response);
				});
		});
	};
};

const changePasswordSuccess = dispatch => {
	dispatch({
		type: CHANGE_PASSWORD_SUCCESS
	});
};

const changePasswordFail = (dispatch, response) => {
	dispatch({
		type: CHANGE_PASSWORD_FAIL,
		payload: response
	});
};

export const newPasswordConfirmationChanged = text => {
	return {
		type: NEW_PASSWORD_CONFIRMATION_CHANGED,
		payload: text
	};
};

export const newPasswordChanged = text => {
	return {
		type: NEW_PASSWORD_CHANGED,
		payload: text
	};
};

export const removeAlert = () => {
	return {
		type: REMOVE_ALERT
	};
};

export const logout = () => {
	return dispatch => {
		AsyncStorage.removeItem("wallet:jwt")
			.then(() => {
				dispatch({ type: LOGOUT });
				Actions.auth();
			})
			.catch(error => {
				console.log(error);
			});
	};
};

export const newAddress = () => {
	return dispatch => {
		dispatch({ type: NEW_ADDRESS });

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			axios
				.post(
					"https://ios.bitcoinfast.co/addresses",
					{},
					{
						headers: {
							Authorization: `Bearer ${jwt}`
						}
					}
				)
				.then(response => {
					newAddressSuccess(dispatch, response.data);
				})
				.catch(error => {
					console.log(error.response);
				});
		});
	};
};

export const loadWallet = refresh => {
	return dispatch => {
		if (refresh) {
			dispatch({ type: REFRESH_WALLET });
		}

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			axios
				.get("https://ios.bitcoinfast.co/wallets/feaklfelafeafeaefa", {
					headers: {
						Authorization: `Bearer ${jwt}`
					}
				})
				.then(response => {
					loadSuccess(dispatch, response.data);
				})
				.catch(() => {
					networkFail(dispatch);
				});
		});
	};
};

const networkFail = dispatch => {
	dispatch({
		type: NETWORK_FAIL
	});
};

const loadSuccess = (dispatch, walletData) => {
	dispatch({
		type: LOAD_WALLET_SUCCESS,
		payload: walletData
	});
};

const newAddressSuccess = (dispatch, addressData) => {
	dispatch({
		type: NEW_ADDRESS_SUCCESS,
		payload: addressData
	});
};

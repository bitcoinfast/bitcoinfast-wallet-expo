import { AsyncStorage } from "react-native";
import axios from "axios";
import { Actions } from "react-native-router-flux";

import {
	SEND_FORM_AMOUNT_CHANGED,
	SEND_FORM_ADDRESS_CHANGED,
	SEND_FORM_SUBMITTED,
	SEND_FORM_FAIL,
	SEND_FORM_SUCCESS,
	SEND_FORM_NETWORK_FAIL,
	SEND_FORM_USD_CHANGED
} from "./types";

export const sendFormSubmitted = ({ address, amount }) => {
	return dispatch => {
		dispatch({ type: SEND_FORM_SUBMITTED });

		console.log(address);
		console.log(amount);

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			axios
				.post(
					"https://ios.bitcoinfast.co/transactions",
					{
						transaction: {
							amount,
							address
						}
					},
					{
						headers: {
							Authorization: `Bearer ${jwt}`
						}
					}
				)
				.then(response => {
					Actions.dashboard();
					sendFormSuccess(dispatch, response);
				})
				.catch(({ response }) => {
					if (response) {
						console.log(response);
						const errorMsg = response.data.message;
						sendFormFail(dispatch, errorMsg);
					} else {
						// no internet
						sendFormNetworkFailed(dispatch);
					}
				});
		});
	};
};

const sendFormFail = (dispatch, error) => {
	dispatch({
		type: SEND_FORM_FAIL,
		payload: error
	});
};

const sendFormSuccess = (dispatch, response) => {
	dispatch({
		type: SEND_FORM_SUCCESS,
		payload: response
	});
};

const sendFormNetworkFailed = dispatch => {
	dispatch({
		type: SEND_FORM_NETWORK_FAIL
	});
};

export const sendFormAddressChanged = text => {
	return {
		type: SEND_FORM_ADDRESS_CHANGED,
		payload: text
	};
};

export const sendFormAmountChanged = (value, usdRate) => {
	const values = {};
	const usdValue = parseFloat(value) * parseFloat(usdRate);
	values.amount = value;
	values.usd = usdValue ? usdValue.toFixed(2) : "";
	return dispatch => {
		dispatch({
			type: SEND_FORM_AMOUNT_CHANGED,
			payload: values
		});
	};
};

export const sendFormUSDChanged = (value, usdRate) => {
	const values = {};
	const coinValue = parseFloat(value) / parseFloat(usdRate);
	values.amount = coinValue ? coinValue.toFixed(2) : "";
	values.usd = value.toString();
	return {
		type: SEND_FORM_USD_CHANGED,
		payload: values
	};
};

import { AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import axios from "axios";
import {
	LOGIN_EMAIL_CHANGED,
	LOGIN_PASSWORD_CHANGED,
	LOGIN_FORM_SUBMITTED,
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	NETWORK_FAIL,
	LOGIN_AUTHENTICATOR_CODE_CHANGED,
	LOGIN_REQUIRE_AUTHENTICATOR_CODE,
	LOGIN_AUTHENTICATOR_FAIL
} from "./types";

export const authenticatorCodeChanged = text => {
	return {
		type: LOGIN_AUTHENTICATOR_CODE_CHANGED,
		payload: text
	};
};

export const loginEmailChanged = text => {
	return {
		type: LOGIN_EMAIL_CHANGED,
		payload: text
	};
};

export const loginPasswordChanged = text => {
	return {
		type: LOGIN_PASSWORD_CHANGED,
		payload: text
	};
};

export const loginFormWithAuthenticatorCodeSubmitted = ({
	email,
	password,
	authenticatorCode
}) => {
	return dispatch => {
		dispatch({ type: LOGIN_FORM_SUBMITTED });

		axios
			.post("https://ios.bitcoinfast.co/wallet_token", {
				auth: {
					email,
					password,
					"2fa_code": authenticatorCode
				}
			})
			.then(response => {
				const jwt = response.data.jwt;
				AsyncStorage.setItem("wallet:jwt", jwt);
				loginSuccess(dispatch);
			})
			.catch(({ response }) => {
				if (response) {
					console.log(response);
					invalidAuthenticatorCode(dispatch);
				} else {
					// no internet connection
					networkFail(dispatch);
				}
			});
	};
};

const invalidAuthenticatorCode = dispatch => {
	dispatch({
		type: LOGIN_AUTHENTICATOR_FAIL
	});
};

export const loginFormSubmitted = ({ email, password }) => {
	return dispatch => {
		dispatch({ type: LOGIN_FORM_SUBMITTED });

		axios
			.post("https://ios.bitcoinfast.co/wallet_token", {
				auth: {
					email,
					password
				}
			})
			.then(response => {
				const jwt = response.data.jwt;
				AsyncStorage.setItem("wallet:jwt", jwt);
				loginSuccess(dispatch);
			})
			.catch(({ response }) => {
				if (response) {
					console.log(response);
					if (response.status === 401) {
						// 2fa required
						requireAuthenticatorCode(dispatch);
					} else {
						// wrong email/password
						const error = response.data;
						loginFail(dispatch, error);
					}
				} else {
					// no internet connection
					networkFail(dispatch);
				}
			});
	};
};

const requireAuthenticatorCode = dispatch => {
	dispatch({
		type: LOGIN_REQUIRE_AUTHENTICATOR_CODE
	});
	Actions.twoFactor();
};

const networkFail = dispatch => {
	dispatch({
		type: NETWORK_FAIL
	});
};

const loginFail = dispatch => {
	dispatch({ type: LOGIN_FAIL });
};

const loginSuccess = dispatch => {
	dispatch({ type: LOGIN_SUCCESS });
	Actions.main({ type: "reset" });
};

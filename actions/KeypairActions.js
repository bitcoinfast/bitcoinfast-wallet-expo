import { AsyncStorage } from "react-native";
import axios from "axios";
import { LOAD_KEYPAIR, LOAD_KEYPAIR_SUCCESS } from "./types";

export const loadKeypair = address => {
	return dispatch => {
		dispatch({ type: LOAD_KEYPAIR });

		AsyncStorage.getItem("wallet:jwt", (err, jwt) => {
			axios
				.get(`https://ios.bitcoinfast.co/addresses/${address}`, {
					headers: {
						Authorization: `Bearer ${jwt}`
					}
				})
				.then(response => {
					dispatch({
						type: LOAD_KEYPAIR_SUCCESS,
						payload: response
					});
				})
				.catch(({ response }) => {
					console.log(response);
				});
		});
	};
};

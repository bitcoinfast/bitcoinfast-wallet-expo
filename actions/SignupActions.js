import { AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import axios from "axios";
import {
	SIGNUP_EMAIL_CHANGED,
	SIGNUP_PASSWORD_CHANGED,
	SIGNUP_PASSWORD_CONFIRMATION_CHANGED,
	SIGNUP_TOS_CHANGED,
	SIGNUP_FORM_SUBMITTED,
	SIGNUP_FAIL,
	SIGNUP_SUCCESS,
	NETWORK_FAIL
} from "./types";

export const signupEmailChanged = text => {
	return {
		type: SIGNUP_EMAIL_CHANGED,
		payload: text
	};
};

export const signupPasswordChanged = text => {
	return {
		type: SIGNUP_PASSWORD_CHANGED,
		payload: text
	};
};

export const signupPasswordConfirmationChanged = text => {
	return {
		type: SIGNUP_PASSWORD_CONFIRMATION_CHANGED,
		payload: text
	};
};

export const signupTosChanged = text => {
	return {
		type: SIGNUP_TOS_CHANGED
	};
};

export const signupFormSubmitted = ({
	email,
	password,
	passwordConfirmation,
	tos
}) => {
	return dispatch => {
		dispatch({ type: SIGNUP_FORM_SUBMITTED });

		axios
			.post("https://ios.bitcoinfast.co/wallets", {
				wallet: {
					email,
					password,
					password_confirmation: passwordConfirmation,
					terms_of_service: tos
				}
			})
			.then(response => {
				const jwt = response.data.jwt;
				AsyncStorage.setItem("wallet:jwt", jwt);

				signupSuccess(dispatch);
			})
			.catch(({ response }) => {
				if (response) {
					console.log(response);
					const error = response.data;
					signupFail(dispatch, error);
				} else {
					// no internet connection
					networkFail(dispatch);
				}
			});
	};
};

const networkFail = dispatch => {
	dispatch({
		type: NETWORK_FAIL
	});
};

const signupFail = (dispatch, error) => {
	dispatch({ type: SIGNUP_FAIL, payload: error });
};

const signupSuccess = dispatch => {
	dispatch({ type: SIGNUP_SUCCESS });
	Actions.main({ type: "reset" });
};

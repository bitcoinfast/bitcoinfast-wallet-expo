import React, { Component } from "react";
import { Scene, Router, Stack, Actions } from "react-native-router-flux";
import Welcome from "./components/Welcome";
import Signup from "./components/Signup";
import Login from "./components/Login";
import Tos from "./components/Tos";
import Dashboard from "./components/Dashboard";
import Settings from "./components/Settings";
import KeyPair from "./components/KeyPair";
import TwoFactor from "./components/TwoFactor";
import Scanner from "./components/Scanner";
import Send from "./components/Send";
import Receive from "./components/Receive";

class RouterComponent extends Component {
	render() {
		return (
			<Router
				navigationBarStyle={{ backgroundColor: "#db0011" }}
				titleStyle={{ color: "white" }}
				headerTintColor="white"
			>
				<Stack key="root" hideNavBar>
					<Scene key="auth">
						<Scene key="welcome" component={Welcome} hideNavBar initial />

						<Scene key="login" title="Log In" component={Login} />

						<Scene
							key="twoFactor"
							title="2-Step Verification"
							component={TwoFactor}
						/>

						<Scene key="signup" title="Create New Wallet" component={Signup} />

						<Scene key="Tos" title="Terms of Service" component={Tos} />
					</Scene>

					<Scene key="main">
						<Scene
							key="dashboard"
							component={Dashboard}
							initial
							title="Dashboard"
							rightTitle=" Settings"
							onRight={() => Actions.settings()}
						/>

						<Scene key="settings" component={Settings} title="Settings" />

						<Scene key="keypair" component={KeyPair} title="Address Info" />

						<Scene key="scanner" component={Scanner} title="QR Scanner" />

						<Scene key="send" component={Send} title="Send" />

						<Scene key="receive" component={Receive} title="Receive" />
					</Scene>
				</Stack>
			</Router>
		);
	}
}

export default RouterComponent;

import React, { Component } from "react";
import { View } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
  CheckBox,
  Button
} from "react-native-elements";
import { connect } from "react-redux";

import {
  signupEmailChanged,
  signupPasswordChanged,
  signupPasswordConfirmationChanged,
  signupTosChanged,
  signupFormSubmitted
} from "../actions";

class Signup extends Component {
  onEmailChange(text) {
    this.props.signupEmailChanged(text);
  }

  onPasswordChange(text) {
    this.props.signupPasswordChanged(text);
  }

  onPasswordConfirmationChange(text) {
    this.props.signupPasswordConfirmationChanged(text);
  }

  onTOSChanged() {
    this.props.signupTosChanged();
  }

  onSubmit() {
    const { email, password, passwordConfirmation, tos } = this.props;

    this.props.signupFormSubmitted({
      email,
      password,
      passwordConfirmation,
      tos
    });
  }

  render() {
    return (
      <View>
        <FormLabel>Email</FormLabel>
        <FormInput
          onChangeText={this.onEmailChange.bind(this)}
          placeholder="email@domain.com"
          value={this.props.email}
          autoCapitalize="none"
          inputStyle={{ width: "100%" }}
          underlineColorAndroid="#cccccc"
        />
        <FormValidationMessage>
          {this.props.error.email ? `Email ${this.props.error.email[0]}` : null}
        </FormValidationMessage>

        <FormLabel>Password</FormLabel>
        <FormInput
          secureTextEntry
          onChangeText={this.onPasswordChange.bind(this)}
          placeholder="******"
          value={this.props.password}
          autoCapitalize="none"
          inputStyle={{ width: "100%" }}
          underlineColorAndroid="#cccccc"
        />
        <FormValidationMessage>
          {this.props.error.password
            ? `Password ${this.props.error.password[0]}`
            : null}
        </FormValidationMessage>

        <FormLabel>Repeat your password</FormLabel>
        <FormInput
          secureTextEntry
          onChangeText={this.onPasswordConfirmationChange.bind(this)}
          placeholder="******"
          value={this.props.passwordConfirmation}
          autoCapitalize="none"
          inputStyle={{ width: "100%" }}
          underlineColorAndroid="#cccccc"
        />
        <FormValidationMessage>
          {this.props.error.password_confirmation
            ? `Password confirmation ${
                this.props.error.password_confirmation[0]
              }`
            : null}
        </FormValidationMessage>

        <CheckBox
          title="I have read and agree to the Terms of Service"
          checked={this.props.tos}
          onPress={this.onTOSChanged.bind(this)}
        />
        <FormValidationMessage>
          {this.props.error.terms_of_service
            ? `Terms of Service ${this.props.error.terms_of_service[0]}`
            : null}
        </FormValidationMessage>

        <View style={styles.buttonWrapper}>
          <Button
            raised
            title="Terms of Service"
            backgroundColor="#ffc057"
            color="#fff"
            borderRadius={4}
            onPress={Actions.Tos}
          />
        </View>

        <View style={styles.buttonWrapper}>
          <Button
            raised
            title="Create New Wallet"
            backgroundColor="#ffc057"
            color="#fff"
            borderRadius={4}
            onPress={this.onSubmit.bind(this)}
            loading={this.props.loading}
            disabled={this.props.loading}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  buttonWrapper: {
    marginBottom: 10
  }
};

const mapStateToProps = ({ signup }) => {
  const { email, password, passwordConfirmation, tos, error, loading } = signup;
  return { email, password, passwordConfirmation, tos, error, loading };
};

export default connect(
  mapStateToProps,
  {
    signupEmailChanged,
    signupPasswordChanged,
    signupPasswordConfirmationChanged,
    signupTosChanged,
    signupFormSubmitted
  }
)(Signup);

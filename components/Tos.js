import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Tos extends Component {
  render() {
    return (
      <View style={styles.containerStyle}>
        <Text style={styles.disclaimer}>
          Disclaimer
        </Text>

        <Text style={styles.text}>
          Always backup your private keys: Our service is not responsible for the keeping and security of your private keys and cannot recover your private keys. You are solely responsible for keeping and maintaining the security of the private keys associated with your wallet. Failure to do so may result in the loss of control of the funds associated with the wallet.
        </Text>

        <Text style={styles.text}>
          We are not responsible for any loss: Some of the underlying libraries we use are under active development. While we have thoroughly tested & tens of thousands of wallets have been successfully created by people all over the globe, there is always the possibility something unexpected happens that causes your funds to be lost. Please do not invest more than you are willing to lose, and please be careful.
        </Text>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15
  },
  disclaimer: {
    fontSize: 20,
    marginBottom: 10
  },
  text: {
    marginBottom: 10
  }
};

export default Tos;

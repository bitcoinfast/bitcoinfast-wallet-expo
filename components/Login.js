import React, { Component } from "react";
import { View } from "react-native";
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
  Button
} from "react-native-elements";
import { connect } from "react-redux";

import {
  loginEmailChanged,
  loginPasswordChanged,
  loginFormSubmitted
} from "../actions";

class Login extends Component {
  onEmailChange(text) {
    this.props.loginEmailChanged(text);
  }

  onPasswordChange(text) {
    this.props.loginPasswordChanged(text);
  }

  onSubmit() {
    const { email, password } = this.props;

    this.props.loginFormSubmitted({ email, password });
  }

  render() {
    return (
      <View>
        <View style={styles.fields}>
          <FormLabel>Email</FormLabel>
          <FormInput
            onChangeText={this.onEmailChange.bind(this)}
            placeholder="email@domain.com"
            autoCapitalize="none"
            inputStyle={{ width: "100%" }}
            value={this.props.email}
            underlineColorAndroid="#cccccc"
          />

          <FormLabel>Password</FormLabel>
          <FormInput
            secureTextEntry
            onChangeText={this.onPasswordChange.bind(this)}
            placeholder="******"
            autoCapitalize="none"
            inputStyle={{ width: "100%" }}
            value={this.props.password}
            underlineColorAndroid="#cccccc"
          />
        </View>

        <FormValidationMessage>{this.props.error}</FormValidationMessage>

        <View style={styles.buttonWrapper}>
          <Button
            raised
            title="Log In"
            backgroundColor="#ffc057"
            color="#fff"
            borderRadius={4}
            onPress={this.onSubmit.bind(this)}
            loading={this.props.loading}
            disabled={this.props.loading}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  buttonWrapper: {
    marginBottom: 10
  }
};

const mapStateToProps = ({ login }) => {
  const { email, password, error, loading } = login;
  return { email, password, error, loading };
};

export default connect(
  mapStateToProps,
  {
    loginEmailChanged,
    loginPasswordChanged,
    loginFormSubmitted
  }
)(Login);

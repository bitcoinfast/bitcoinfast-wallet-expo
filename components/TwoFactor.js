import React, { Component } from "react";
import { View } from "react-native";
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
  Button
} from "react-native-elements";
import { connect } from "react-redux";

import {
  authenticatorCodeChanged,
  loginFormWithAuthenticatorCodeSubmitted,
  unmountTwoFactor
} from "../actions";

class TwoFactor extends Component {
  componentWillUnmount = () => {
    this.props.unmountTwoFactor();
  };

  onSubmit = () => {
    const { email, password, authenticatorCode } = this.props.login;
    this.props.loginFormWithAuthenticatorCodeSubmitted({
      email,
      password,
      authenticatorCode
    });
  };

  render() {
    return (
      <View>
        <View style={styles.fields}>
          <FormLabel>Google authenticator code</FormLabel>
          <FormInput
            onChangeText={text => this.props.authenticatorCodeChanged(text)}
            placeholder="Enter your code"
            autoCapitalize="none"
            inputStyle={{ width: "100%" }}
            value={this.props.login.authenticatorCode}
            underlineColorAndroid="#cccccc"
          />
        </View>

        <FormValidationMessage>
          {this.props.login.authenticatorError}
        </FormValidationMessage>

        <View style={styles.buttonWrapper}>
          <Button
            title="Log In"
            backgroundColor="#ffc057"
            color="#fff"
            borderRadius={4}
            onPress={() => this.onSubmit()}
            loading={this.props.login.loading}
            disabled={this.props.login.loading}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  buttonWrapper: {
    marginBottom: 10
  }
};

const mapStateToProps = ({ login }) => {
  return { login };
};

export default connect(
  mapStateToProps,
  {
    authenticatorCodeChanged,
    loginFormWithAuthenticatorCodeSubmitted,
    unmountTwoFactor
  }
)(TwoFactor);

import React, { Component } from "react";
import { Platform, View, ScrollView } from "react-native";
import {
  Card,
  FormLabel,
  FormInput,
  Button,
  List,
  ListItem,
  Text,
  FormValidationMessage
} from "react-native-elements";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { ClipboardCopiable } from "./common";

import {
  loadWallet,
  logout,
  newPasswordChanged,
  newPasswordConfirmationChanged,
  changePasswordSubmitted,
  importFormTextChanged,
  importFormSubmitted,
  twoFactorToggled
} from "../actions";

class Settings extends Component {
  onNewPasswordConfirmationChange = text => {
    this.props.newPasswordConfirmationChanged(text);
  };

  onNewPasswordChange = text => {
    this.props.newPasswordChanged(text);
  };

  onChangePasswordSubmit = () => {
    const { newPassword, newPasswordConfirmation } = this.props.wallet;
    this.props.changePasswordSubmitted({
      newPassword,
      newPasswordConfirmation
    });
  };

  onTwoFactorChange = () => {
    const toggled = this.props.wallet.twoFactorEnabled;
    this.props.twoFactorToggled(toggled);
  };

  onPrivateKeyChange = text => {
    this.props.importFormTextChanged(text);
  };

  onImportPrivateKeySubmit = () => {
    this.props.importFormSubmitted(this.props.wallet.importFormText);
  };

  renderAddresses = () => {
    if (typeof this.props.wallet.addresses === "string") {
      return (
        <List
          containerStyle={{
            marginBottom: 20,
            marginTop: 0,
            borderColor: "transparent"
          }}
        >
          <Text style={[styles.output, { textAlign: "center" }]}>
            {this.props.wallet.addresses}
          </Text>
        </List>
      );
    } else if (this.props.wallet.addresses.length === 0) {
      return (
        <List
          containerStyle={{
            marginBottom: 20,
            marginTop: 0,
            borderColor: "transparent"
          }}
        >
          <Text style={[styles.output, { textAlign: "center" }]}>
            No addresses.
          </Text>
        </List>
      );
    } else {
      return (
        <List
          containerStyle={{
            marginBottom: 20,
            marginTop: 0,
            borderColor: "transparent"
          }}
        >
          {this.props.wallet.addresses.map((addr, index) => (
            <ListItem
              title={addr}
              key={index}
              onPress={() => Actions.keypair({ routerPassedAddress: addr })}
            />
          ))}
        </List>
      );
    }
  };

  logout = () => {
    this.props.logout();
  };

  renderBackupKey = () => {
    if (this.props.wallet.backupKey) {
      return (
        <View>
          <Text
            style={[styles.output, { textAlign: "center", marginBottom: 5 }]}
          >
            Backup key:
          </Text>
          <ClipboardCopiable style={[styles.output, { textAlign: "center" }]}>
            {this.props.wallet.backupKey}
          </ClipboardCopiable>
        </View>
      );
    }
  };

  render() {
    return (
      <ScrollView style={{ flex: 1 }} keyboardDismissMode="on-drag">
        <Card title="Disconnect this device">
          <View style={{ marginBottom: 5 }}>
            <Button
              raised
              title="Log out"
              backgroundColor="#ffc057"
              color="#fff"
              borderRadius={4}
              onPress={this.logout.bind(this)}
            />
          </View>
        </Card>

        <Card title="Change password">
          <View style={styles.inputWrapper}>
            <FormLabel>New password</FormLabel>
            <FormInput
              secureTextEntry
              onChangeText={this.onNewPasswordChange.bind(this)}
              placeholder="******"
              autoCapitalize="none"
              inputStyle={{ width: "100%" }}
              value={this.props.wallet.newPassword}
              underlineColorAndroid="#cccccc"
            />
            <FormValidationMessage>
              {this.props.wallet.changePasswordError.password
                ? `Password ${
                    this.props.wallet.changePasswordError.password[0]
                  }`
                : null}
            </FormValidationMessage>

            <FormLabel>Repeat new password</FormLabel>
            <FormInput
              secureTextEntry
              onChangeText={this.onNewPasswordConfirmationChange.bind(this)}
              placeholder="******"
              autoCapitalize="none"
              inputStyle={{ width: "100%" }}
              value={this.props.wallet.newPasswordConfirmation}
              underlineColorAndroid="#cccccc"
            />
            <FormValidationMessage>
              {this.props.wallet.changePasswordError.password_confirmation
                ? `Password ${
                    this.props.wallet.changePasswordError
                      .password_confirmation[0]
                  }`
                : null}
            </FormValidationMessage>
          </View>

          <View style={{ marginBottom: 5 }}>
            <Button
              raised
              title="Save"
              backgroundColor="#ffc057"
              color="#fff"
              borderRadius={4}
              onPress={this.onChangePasswordSubmit.bind(this)}
              loading={this.props.wallet.changePasswordLoading}
              disabled={this.props.wallet.changePasswordLoading}
            />
          </View>
        </Card>

        <Card title="Google Authenticator 2FA">
          <List
            containerStyle={{
              marginBottom: 20,
              marginTop: 0,
              borderColor: "transparent"
            }}
          >
            <ListItem
              title="Google Authenticator"
              subtitle="2FA authentication"
              hideChevron
              switchButton
              switched={this.props.wallet.twoFactorEnabled}
              switchOnTintColor="#dc3545"
              onSwitch={this.onTwoFactorChange}
            />
          </List>

          {this.renderBackupKey()}
        </Card>

        <Card title="Addresses">
          <List
            containerStyle={{
              marginBottom: 20,
              marginTop: 0,
              borderColor: "transparent"
            }}
          >
            {this.renderAddresses()}
          </List>
        </Card>

        <View style={{ margin: 30 }} />
      </ScrollView>
    );
  }
}

const fontFamily = Platform.OS === "ios" ? "Courier" : "monospace";

const styles = {
  inputWrapper: {
    marginBottom: 20
  },
  output: {
    color: "#ffc057",
    fontFamily
  }
};

const mapStateToProps = ({ wallet }) => {
  return { wallet };
};

export default connect(
  mapStateToProps,
  {
    loadWallet,
    logout,
    newPasswordChanged,
    newPasswordConfirmationChanged,
    changePasswordSubmitted,
    importFormTextChanged,
    importFormSubmitted,
    twoFactorToggled
  }
)(Settings);

import { Actions } from "react-native-router-flux";
import React, { Component } from "react";
import { View, ScrollView, Platform, RefreshControl } from "react-native";
import {
  Card,
  Text,
  Icon,
  Button,
  List,
  ListItem,
  FormValidationMessage
} from "react-native-elements";
import { connect } from "react-redux";
import timeago from "timeago.js";
import QRCode from "react-native-qrcode";
import { ClipboardCopiable } from "./common";
import {
  loadWallet,
  newAddress,
  sendFormAddressChanged,
  sendFormAmountChanged,
  sendFormSubmitted,
  openScanner
} from "../actions";

class Dashboard extends Component {
  componentWillMount = () => {
    this.props.loadWallet();
  };

  onSendAddressChange = text => {
    this.props.sendFormAddressChanged(text);
  };

  onSendAmountChange = text => {
    this.props.sendFormAmountChanged(text);
  };

  onSendSubmit = () => {
    const { address, amount } = this.props.sendForm;
    this.props.sendFormSubmitted({ address, amount });
  };

  renderTransactions = () => {
    if (typeof this.props.wallet.transactions === "string") {
      return (
        <List
          containerStyle={{
            marginBottom: 20,
            marginTop: 0,
            borderColor: "transparent"
          }}
        >
          <Text style={[styles.output, { textAlign: "center" }]}>
            {this.props.wallet.transactions}
          </Text>
        </List>
      );
    } else if (this.props.wallet.transactions.length === 0) {
      return (
        <List
          containerStyle={{
            marginBottom: 20,
            marginTop: 0,
            borderColor: "transparent"
          }}
        >
          <Text style={[styles.output, { textAlign: "center" }]}>
            No transactions.
          </Text>
        </List>
      );
    } else {
      const timeagoInstance = timeago();

      return (
        <List
          containerStyle={{
            marginBottom: 20,
            marginTop: 0,
            borderColor: "transparent"
          }}
        >
          {this.props.wallet.transactions.map((tx, index) => {
            return (
              <ListItem
                title={tx.address}
                subtitle={timeagoInstance.format(tx.time * 1000)}
                hideChevron
                titleStyle={styles[`${tx.category}Title`]}
                rightTitle={`${tx.amount} BCF`}
                rightTitleStyle={styles[`${tx.category}RightTitle`]}
                key={index}
              />
            );
          })}
        </List>
      );
    }
  };

  onRefresh = () => {
    this.props.loadWallet(true);
  };

  onNewAddressPress = () => {
    this.props.newAddress();
  };

  // renderQrcode = () => {
  //  if (this.props.wallet.address === "loading...") return null;

  //  return (
  //    <View style={styles.qrcodeWrapper}>
  //      <QRCode
  //        value={this.props.wallet.address}
  //        size={200}
  //        bgColor="#ffc057"
  //        fgColor="white"
  //      />
  //    </View>
  //  );
  // };

  renderAddress = () => {
    if (this.props.wallet.address === "loading...") {
      return (
        <View style={{ marginTop: 10 }}>
          <Text style={styles.output}>{this.props.wallet.address}</Text>
        </View>
      );
    }

    return (
      <View style={{ marginTop: 10 }}>
        <ClipboardCopiable style={[styles.output, { textAlign: "center" }]}>
          {this.props.wallet.address}
        </ClipboardCopiable>

        <Icon
          name="refresh"
          color="#777"
          size={20}
          onPress={this.onNewAddressPress.bind(this)}
        />
      </View>
    );
  };

  renderBalance = () => {
    let balance = `${parseFloat(this.props.wallet.balance).toFixed(2)} BCF`;
    let usdBalance = `$${parseFloat(this.props.wallet.usdBalance).toFixed(2)}`;

    if (this.props.wallet.loading) {
      balance = usdBalance = "loading...";
    }

    return (
      <View>
        <Text style={styles.heading}>Wallet Balance</Text>
        <Text style={[styles.output, { fontSize: 30 }]}>{balance}</Text>
      </View>
    );
  };

  render() {
    return (
      <ScrollView
        style={{ flex: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={this.props.wallet.refreshing}
            onRefresh={this.onRefresh.bind(this)}
          />
        }
        keyboardDismissMode="on-drag"
      >
        <Card
          title="Wallet Information"
          containerStyle={{ flex: 1 }}
          wrapperStyle={{ flex: 1 }}
        >
          {this.renderBalance()}

          <View style={styles.buttonWrapper}>
            <View style={{ flex: 1 }}>
              <Button
                title="Send"
                color="#fff"
                backgroundColor="#ffc057"
                borderRadius={4}
                onPress={Actions.send}
                raised
              />
            </View>

            <View style={{ flex: 1 }}>
              <Button
                title="Receive"
                color="#fff"
                backgroundColor="#ffc057"
                borderRadius={4}
                onPress={Actions.receive}
                raised
              />
            </View>
          </View>
        </Card>

        <Card title="Transaction history">{this.renderTransactions()}</Card>

        <View style={{ margin: 30 }} />
      </ScrollView>
    );
  }
}

const fontFamily = Platform.OS === "ios" ? "Courier" : "monospace";

const styles = {
  qrcodeWrapper: {
    marginTop: 5,
    marginBottom: 5
  },
  receiveRightTitle: {
    textAlign: "center",
    width: "90%",
    color: "white",
    backgroundColor: "#28a745",
    padding: 10,
    borderRadius: 4,
    overflow: "hidden"
  },
  sendRightTitle: {
    textAlign: "center",
    width: "90%",
    color: "white",
    backgroundColor: "#dc3545",
    padding: 10,
    borderRadius: 4,
    overflow: "hidden"
  },
  sendTitle: {
    color: "#dc3545"
  },
  receiveTitle: {
    color: "#28a745"
  },
  textWithIcon: {
    flex: 1,
    flexDirection: "row"
  },
  output: {
    color: "#ffc057",
    textAlign: "center"
  },
  inputWrapper: {
    marginBottom: 20
  },
  heading: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 5,
    textAlign: "center"
  },
  cardSection: {
    marginBottom: 20,
    flex: 1
  },
  buttonWrapper: {
    flex: 1,
    flexDirection: "row",
    marginTop: 50,
    marginBottom: 5
  }
};

const mapStateToProps = ({ wallet, sendForm }) => {
  return { wallet, sendForm };
};

export default connect(
  mapStateToProps,
  {
    loadWallet,
    newAddress,
    sendFormAddressChanged,
    sendFormAmountChanged,
    sendFormSubmitted,
    openScanner
  }
)(Dashboard);

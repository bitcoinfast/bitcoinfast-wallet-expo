import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import {
  Card,
  Button,
  FormLabel,
  FormInput,
  FormValidationMessage
} from "react-native-elements";
import { connect } from "react-redux";
import {
  sendFormAddressChanged,
  sendFormAmountChanged,
  sendFormSubmitted,
  openScanner,
  sendFormUSDChanged
} from "../actions";

class Send extends Component {
  onSendAddressChange = text => {
    this.props.sendFormAddressChanged(text);
  };

  onSendAmountChange = value => {
    const { usdRate } = this.props.wallet;
    this.props.sendFormAmountChanged(value, usdRate);
  };

  onSendSubmit = () => {
    const { address, amount } = this.props.sendForm;
    this.props.sendFormSubmitted({ address, amount });
  };

  render() {
    return (
      <ScrollView style={{ flex: 1 }} keyboardDismissMode="on-drag">
        <Card title="Send BCF">
          <View
            style={{
              marginBottom: 20
            }}
          >
            <View style={{ marginBottom: 5 }}>
              <Button
                raised
                title="Scan a QR code"
                backgroundColor="#ffc057"
                color="#fff"
                borderRadius={4}
                rightIcon={{
                  name: "crop-free"
                }}
                onPress={this.props.openScanner}
              />
            </View>

            <FormLabel>Receiving address</FormLabel>
            <FormInput
              onChangeText={this.onSendAddressChange.bind(this)}
              placeholder="Enter address"
              autoCapitalize="none"
              inputStyle={{
                width: "100%"
              }}
              underlineColorAndroid="#cccccc"
              value={this.props.sendForm.address}
            />

            <View
              style={{ flexDirection: "row", marginLeft: 15, marginRight: 15 }}
            >
              <View style={{ flex: 1 }}>
                <FormLabel labelStyle={{ marginLeft: 0, marginRight: 0 }}>
                  Amount (BCF)
                </FormLabel>
                <FormInput
                  onChangeText={this.onSendAmountChange.bind(this)}
                  placeholder="0.00"
                  autoCapitalize="none"
                  inputStyle={{ width: "100%" }}
                  value={this.props.sendForm.amount}
                  containerStyle={{ marginLeft: 0, marginRight: 0 }}
                  underlineColorAndroid="#cccccc"
                />
              </View>
            </View>
          </View>

          <FormValidationMessage>
            {this.props.sendForm.error ? this.props.sendForm.error : null}
          </FormValidationMessage>

          <View style={{ marginBottom: 5 }}>
            <Button
              raised
              title="Send (Fee: 0.01 BCF)"
              backgroundColor="#ffc057"
              color="#fff"
              borderRadius={4}
              onPress={this.onSendSubmit.bind(this)}
              loading={this.props.sendForm.loading}
              disabled={this.props.sendForm.loading}
            />
          </View>
        </Card>
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ sendForm, wallet }) => {
  return { sendForm, wallet };
};

const mapDispatchToProps = {
  sendFormAddressChanged,
  sendFormAmountChanged,
  sendFormSubmitted,
  openScanner,
  sendFormUSDChanged
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Send);

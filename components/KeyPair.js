import React, { Component } from 'react';
import {
  View,
  Platform
} from 'react-native';
import {
  Card,
  Text,
} from 'react-native-elements';
import { connect } from 'react-redux';

import {
  ClipboardCopiable
} from './common';
import {
  loadKeypair
} from '../actions';

class KeyPair extends Component {
  componentWillMount = () => {
    this.props.loadKeypair(this.props.routerPassedAddress);
  }

  renderAddress = () => {
    if (this.props.keypair.address === 'loading...') {
      return (
        <Text style={styles.output}>
          {this.props.keypair.address}
        </Text>
      );
    }

    return (
      <ClipboardCopiable style={styles.output}>
        {this.props.keypair.address}
      </ClipboardCopiable>
    );
  }

  renderPrivateKey = () => {
    if (this.props.keypair.privateKey === 'loading...') {
      return (
        <Text style={styles.output}>
          {this.props.keypair.privateKey}
        </Text>
      );
    }

    return (
      <ClipboardCopiable style={styles.output}>
        {this.props.keypair.privateKey}
      </ClipboardCopiable>
    );
  }

  render() {
    return (
      <Card title="Address Information">
        <View style={styles.cardSection}>
          <Text style={styles.heading}>
            Address
          </Text>
          <Text style={styles.output}>
            {this.renderAddress()}
          </Text>
        </View>

        <View style={styles.cardSection}>
          <Text style={styles.heading}>
            Private key
          </Text>
          <Text style={styles.output}>
            {this.renderPrivateKey()}
          </Text>
        </View>
      </Card>
    );
  }
}

const fontFamily = Platform.OS === 'ios' ? 'Courier' : 'monospace';

const styles = {
  output: {
    color: '#ffc057',
    fontFamily
  },
  heading: {
    fontWeight: 'bold', fontSize: 20, marginBottom: 5
  },
  cardSection: {
    marginBottom: 20
  }
};

const mapStateToProps = ({ keypair }) => {
  return { keypair };
};

export default connect(mapStateToProps, {
  loadKeypair
})(KeyPair);

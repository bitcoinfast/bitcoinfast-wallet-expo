import React, { Component } from "react";
import { View, Text, Clipboard, Animated } from "react-native";

class ClipboardCopiable extends Component {
	constructor(props) {
		super(props);
		const { style, children } = this.props;

		this.state = {
			style,
			children
		};
	}

	onTextPress = children => {
		const originalText = children;
		Clipboard.setString(children);

		// animate to 'copied to clipboard' then animate back
		this.setState({ children: "Copied to clipboard." }, () => {
			setTimeout(() => this.setState({ children: originalText }), 1000);
		});
	};

	render() {
		const { style, children } = this.state;

		return (
			<Text style={style} onPress={() => this.onTextPress(children)}>
				{children}
			</Text>
		);
	}
}

export { ClipboardCopiable };

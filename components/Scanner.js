import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { BarCodeScanner, Permissions } from "expo";
import { setAddressAndReturn } from "../actions";
import { connect } from "react-redux";

import { Actions } from "react-native-router-flux";

class Scanner extends Component {
	state = {
		hasCameraPermission: null,
		active: true
	};

	async componentWillMount() {
		this.setState({ active: true });
		const { status } = await Permissions.askAsync(Permissions.CAMERA);
		this.setState({ hasCameraPermission: status === "granted" });
	}

	handleBarCodeScanned = async ({ type, data }) => {
		this.setState({ active: false });
		this.props.setAddressAndReturn(data);
	};

	renderScanner = () => {
		if (this.state.active) {
			return (
				<BarCodeScanner
					onBarCodeScanned={this.handleBarCodeScanned}
					style={StyleSheet.absoluteFill}
				/>
			);
		}
	};

	render() {
		const { hasCameraPermission } = this.state;

		if (hasCameraPermission === null) {
			return <Text>Requesting for camera permission</Text>;
		}

		if (hasCameraPermission === false) {
			return <Text>No access to camera</Text>;
		}

		return <View style={{ flex: 1 }}>{this.renderScanner()}</View>;
	}
}

const mapStateToProps = () => {
	return {};
};
const mapFunctionsToProps = {
	setAddressAndReturn
};

export default connect(
	mapStateToProps,
	mapFunctionsToProps
)(Scanner);

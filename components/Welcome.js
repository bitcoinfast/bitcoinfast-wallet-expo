import React, { Component } from "react";
import { View, ImageBackground, Image, Text } from "react-native";
import { Button } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import { AsyncStorage } from "react-native";

class Welcome extends Component {
	state = { render: false };

	componentWillMount() {
		this.authenticateUser();
	}

	async authenticateUser() {
		const jwt = await AsyncStorage.getItem("wallet:jwt");

		if (jwt) {
			Actions.main({ type: "reset" });
		} else {
			this.setState({ render: true });
		}
	}

	render() {
		if (!this.state.render) return null;

		return (
			<View style={styles.containerStyle}>
				<ImageBackground
					source={require("../assets/2.png")}
					style={styles.backgroundImage}
				>
					<Image
						source={require("../assets/bcf.png")}
						style={styles.logoStyle}
					/>

					<Text style={styles.textStyle}>
						Global transactions in the blink of an eye.
					</Text>

					<View style={styles.wrapperStyle}>
						<Button
							raised
							title="Create New Wallet"
							backgroundColor="#ffc057"
							color="#fff"
							borderRadius={4}
							onPress={Actions.signup}
						/>
					</View>

					<View style={styles.wrapperStyle}>
						<Button
							raised
							title="Log In"
							backgroundColor="#ffc057"
							color="#fff"
							borderRadius={4}
							onPress={Actions.login}
						/>
					</View>
				</ImageBackground>
			</View>
		);
	}
}

const styles = {
	wrapperStyle: {
		paddingLeft: 60,
		paddingRight: 60,
		marginTop: 10
	},
	descStyle: {
		color: "white",
		textAlign: "center",
		fontSize: 20
	},
	logoStyle: {
		width: 150,
		height: 150,
		alignSelf: "center"
	},
	textStyle: {
		color: "white",
		textAlign: "center",
		marginTop: 20
	},
	containerStyle: {
		flex: 1,
		backgroundColor: "#db0011"
	},
	backgroundImage: {
		flex: 1,
		justifyContent: "center"
	}
};

export default Welcome;

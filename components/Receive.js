import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import { Card, Button, Text, Icon } from "react-native-elements";
import QRCode from "react-native-qrcode";
import { loadWallet, newAddress } from "../actions";
import { ClipboardCopiable } from "./common";
import { connect } from "react-redux";

class Receive extends Component {
	onNewAddressPress = () => {
		this.props.newAddress();
	};

	renderAddress = () => {
		if (this.props.wallet.address === "loading...") {
			return (
				<View style={{ marginTop: 10 }}>
					<Text
						style={{
							color: "#ffc057"
						}}
					>
						{this.props.wallet.address}
					</Text>
				</View>
			);
		}

		return (
			<View style={{ marginTop: 10 }}>
				<ClipboardCopiable
					style={[
						{
							color: "#ffc057"
						},
						{ textAlign: "center" }
					]}
				>
					{this.props.wallet.address}
				</ClipboardCopiable>

				<Icon
					name="refresh"
					color="#777"
					size={20}
					onPress={this.onNewAddressPress.bind(this)}
				/>
			</View>
		);
	};

	render() {
		return (
			<ScrollView style={{ flex: 1 }} keyboardDismissMode="on-drag">
				<Card title="Receive BCF">
					<View
						style={[
							{
								marginBottom: 20,
								flex: 1
							},
							{ justifyContent: "center", alignItems: "center" }
						]}
					>
						<View
							style={[
								{
									flex: 1,
									flexDirection: "row"
								},
								{ marginBottom: 5 }
							]}
						>
							<Text
								style={{
									fontWeight: "bold",
									fontSize: 20,
									marginBottom: 5
								}}
							>
								Wallet Address
							</Text>
						</View>

						<QRCode value={this.props.wallet.address} />

						{this.renderAddress()}
					</View>
				</Card>
			</ScrollView>
		);
	}
}

const mapStateToProps = ({ wallet }) => {
	return { wallet };
};

const mapDispatchToProps = {
	loadWallet,
	newAddress
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Receive);
